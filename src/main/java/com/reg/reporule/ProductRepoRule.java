package com.reg.reporule;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reg.entity.Product;

@Repository
public interface ProductRepoRule extends JpaRepository<Product, Long> {

}
