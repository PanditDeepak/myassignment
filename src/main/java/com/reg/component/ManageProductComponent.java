package com.reg.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.reg.request.ProductRequest;
import com.reg.response.ProductResponse;
import com.reg.service.impl.ProductServiceImpl;

@Component
public class ManageProductComponent {

	@Autowired
	private ProductServiceImpl productService;
	
	public ProductResponse getById(ProductRequest request) {
		return productService.getById(request);
	}

	public ProductResponse getAllProduct() {
		return productService.getAllProduct();
	}
}
