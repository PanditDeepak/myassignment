package com.reg.constant;

public enum UserType {

	PARTICIPANT, VOLUNTEER
}
