package com.reg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reg.component.ManageProductComponent;
import com.reg.request.ProductRequest;
import com.reg.response.ProductResponse;

@RestController
public class MainController {

	@Autowired
	private ManageProductComponent productComponent;
	
	@GetMapping("/getbyId")
	public ProductResponse getById(@RequestBody ProductRequest request) {
		return productComponent.getById(request);
	}

	@GetMapping("/getallproduct")
	public ProductResponse getAllProduct() {
		return productComponent.getAllProduct();
	}
	
}
