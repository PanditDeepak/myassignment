package com.reg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reg.entity.Product;
import com.reg.repository.ManageProductRepository;
import com.reg.request.ProductRequest;
import com.reg.response.ProductResponse;
import com.reg.service.ManageProductService;

@Service
public class ProductServiceImpl implements ManageProductService {

	@Autowired
	private ManageProductRepository productrepository;

	public ProductResponse getById(ProductRequest request) {
		ProductResponse response = new ProductResponse();
		Product product = productrepository.getById(request.getId());
		if (product != null) {
			response.setProduct(product);
			response.setStatus(true);
		}
		return response;
	}

	public ProductResponse getAllProduct() {
		List<Product> products = productrepository.findAll();
		ProductResponse response = new ProductResponse();
		if (products != null) {
			response.setProducts(products);
			response.setStatus(true);
		}
		return response;
	}
}
